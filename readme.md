# Step Project - Cards

Link on Project:
https://mmmmokhov.gitlab.io/3-step-project/

**test email**: 5handsteam@gmail.com<br/>
**test password**: 000111<br/>

**Участники:**

- Oleksandr Mokhov<br/>
- Vladyslav Ralets<br/>
- Yaroslav Gorobets<br/>

**Oleksandr Mokhov**<br/>
Классы компонентов , класс Form, (form Dentist, form Cardiologist, form Therapist) . CardAPI, и info.js.

**Vladyslav Ralets**<br/>
Функционал входа, функционал фильтрации и поиска, SCSS, Bootstrap, HTML. Класс Visit.js

**Yaroslav Gorobets**<br/>
Переключение выбора докторов SelectDoctor, FormSelect, class Modal и подклассы, редактирование и удаление карточек Visit.js и его под классы , рендеринг (renderCards.js) и стилизация карточек, Bootstrap.


**Technologies, used in project:**

- html5
- css3 / scss
- JS ES6
- Bootstrap 5.1.0
- Webpack 5.48.0
- Git
- GitLab Pages
