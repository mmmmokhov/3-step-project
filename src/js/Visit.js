import {deleteCard, editCard} from "./CardAPI";
import FormDentist from "./Form/FormDentist";
import FormCardiologist from "./Form/FormCardiologist";
import FormTherapist from "./Form/FormTherapist";
import {ModalVisit} from "./modal/Modal";
import {filterSelect} from "./Filter";
import {getCards} from "./CardAPI";

export class Visit {
    constructor({id, fullName, doctor, purpose, desc, priority, date}) {
        this.id = id;
        this.doctor = doctor;
        this.fullName = fullName;
        this.purpose = purpose;
        this.priority = priority;
        this.desc = desc;
        this.date = date;
        this.elem = {
            self: document.createElement('div'),
            fullName: document.createElement('h3'),
            doctor: document.createElement('span'),
            purpose: document.createElement('span'),
            desc: document.createElement('p'),
            priority: document.createElement('span'),
            date: document.createElement('span'),
            btnShowMore: document.createElement('button'),
            btnHide: document.createElement('button'),
            btnDelete: document.createElement('div'),
            btnWrapper: document.createElement('div'),
            delBtn: document.createElement('button'),
            editBtn: document.createElement('button')
        };
    };
    render(parent){
        this.elem.fullName.textContent = this.fullName;
        this.elem.doctor.textContent = `Doctor: ${this.doctor}`;
        this.elem.purpose.textContent = `Purpose of the visit: ${this.purpose}`;
        this.elem.desc.textContent = `Description: ${this.desc}`;
        this.elem.priority.textContent = `Priority: ${this.priority}`;
        this.elem.date.textContent = `Visit Date: ${this.date}`;
        this.elem.btnShowMore.textContent = `Show More`;
        this.elem.btnHide.textContent = `Hide`;
        this.elem.btnHide.style.display = 'none';
        this.elem.delBtn.textContent = `Delete`;
        this.elem.editBtn.textContent = `Edit`;

        this.elem.self.id = this.id
        this.elem.self.classList.add('card', 'card-body')
        this.elem.btnShowMore.classList.add('btn', 'btn-success')
        this.elem.btnHide.classList.add('btn', 'btn-warning', 'hide-btn')
        this.elem.delBtn.classList.add('btn', 'btn-outline-danger')
        this.elem.editBtn.classList.add('btn', 'btn-outline-primary')
        this.elem.btnWrapper.classList.add('btn-wrapper')
        this.elem.doctor.classList.add('doctor-title')
        this.elem.btnDelete.classList.add('btn-delete')
        this.elem.btnDelete.innerText = '×';

        this.elem.btnDelete.addEventListener('click', (e) => {
            e.preventDefault();
            this.delete()
        })

        this.elem.delBtn.addEventListener('click', (e) => {
            e.preventDefault();
            this.delete()
        })

        this.elem.btnWrapper.append(this.elem.delBtn, this.elem.editBtn)
        this.elem.self.append(this.elem.btnDelete, this.elem.fullName, this.elem.doctor, this.elem.btnShowMore, this.elem.btnHide, this.elem.btnWrapper);
    }

    delete() {
        const element = document.getElementById(`${this.id}`)
        deleteCard(this.id)
            .then((response) => {
                if(response.ok) {
                    const cardWrapper = document.querySelector('.cardWrapper')
                    cardWrapper.innerHTML = "";
                    filterSelect(getCards())
                }
            })
    }

    filledDoctor(doctor, modalForm) {
        modalForm.style.display = 'block';
        doctor.fullName.value = `${this.fullName}`
        doctor.purpose.value = `${this.purpose}`
        doctor.desc.value = `${this.desc}`
        doctor.priority.value = `${this.priority}`
        doctor.date.value = `${this.date}`
    }

    submitHandler(id) {
        const readData = document.querySelector('.form-doctor-edit');
        readData.addEventListener('submit', (e) => {
            e.preventDefault();
            const formData = new FormData(e.target);
            const data = Array.from(formData.entries()).reduce((memo, pair) => ({
                ...memo,
                [pair[0]]: pair[1],
            }), {});
            editCard(data, id)
                .then((response) => {
                    if (response.ok) {
                        const cardWrapper = document.querySelector('.cardWrapper')
                        cardWrapper.innerHTML = "";
                        filterSelect(getCards())
                    } else {
                        throw new Error('Something went wrong');
                    }

                })
            document.querySelector(".closeBtnEdit").click();
        });

    }
}

export class VisitCardiologist extends Visit {
    constructor({id, fullName, doctor, purpose, desc, priority, date, pressure, bodyMassIndex, diseases, age}) {
        super({id, fullName, doctor, purpose, desc, priority, date});
        this.pressure = pressure;
        this.bodyMassIndex = bodyMassIndex;
        this.diseases = diseases;
        this.age = age;
    }

    render(parent){
        super.render(parent);
        this.elem.pressure = document.createElement('span');
        this.elem.bodyMassIndex = document.createElement('span');
        this.elem.diseases = document.createElement('span');
        this.elem.age = document.createElement('span');

        this.elem.pressure.textContent =`Pressure: ${this.pressure}`;
        this.elem.bodyMassIndex.textContent =`Body mass index: ${this.bodyMassIndex}`;
        this.elem.diseases.textContent = `Past heart disease: ${this.diseases}`;
        this.elem.age.textContent = `Age: ${this.age}`;

        this.elem.btnShowMore.addEventListener('click', () => {
            this.showMore()
        });

        this.elem.btnHide.addEventListener('click', () => {
            this.hide();
        });

        if(parent){
            parent.append(this.elem.self)
        }else {
            return this.elem.self;
        }
        this.edit();
    }

    showMore(){
        const moreInfo =[];

        for (let key in this.elem) {
            if (key === 'purpose' || key === 'desc' || key === 'priority' || key === 'date' || key === 'pressure' || key === 'bodyMassIndex' || key === 'diseases' || key === "age") {
                moreInfo.push(this.elem[key]);
            }
        }
        moreInfo.forEach(item => {
            this.elem.self.insertBefore(item, this.elem.btnShowMore);
        });

        this.elem.btnShowMore.style.display = 'none';
        this.elem.btnHide.style.display = 'inline-block';
    }

    hide() {
        this.elem.self.removeChild(this.elem.purpose);
        this.elem.self.removeChild(this.elem.desc);
        this.elem.self.removeChild(this.elem.priority);
        this.elem.self.removeChild(this.elem.date);
        this.elem.self.removeChild(this.elem.pressure);
        this.elem.self.removeChild(this.elem.bodyMassIndex);
        this.elem.self.removeChild(this.elem.diseases);
        this.elem.self.removeChild(this.elem.age);
        this.elem.btnHide.style.display ='none';
        this.elem.btnShowMore.style.display = 'inline-block';
    }
    filledDoctor(cardio, formWrapper, modalForm) {
        super.filledDoctor(cardio, modalForm)
        cardio.pressure.value = `${this.pressure}`
        cardio.bodyMassIndex.value = `${this.bodyMassIndex}`
        cardio.diseases.value = `${this.diseases}`
        cardio.age.value = `${this.age}`
        cardio.renderEdit(formWrapper, this.id)
    }

    edit() {
        this.elem.editBtn.addEventListener('click', (e) => {
            e.preventDefault()
            const content = document.getElementById('content');
            const modalVisitEdit = new ModalVisit();
            modalVisitEdit.renderModalEdit(content)
            const formWrapper = document.querySelector('.form-wrapper-edit')
            const modalForm = document.querySelector('.form-edit')
            const cardio = new FormCardiologist();
            this.filledDoctor(cardio, formWrapper, modalForm)
            this.submitHandler(this.id)
        })

    }

}

export class VisitDentist extends Visit {
    constructor({id, fullName, doctor, purpose, desc, priority, date, lastDateVisit}) {
        super({id, fullName, doctor, purpose, desc, priority, date});
        this.lastDateVisit = lastDateVisit;
    }

    render(parent){
        super.render(parent);
        this.elem.lastDateVisit = document.createElement('span');
        this.elem.lastDateVisit.textContent =`Date of last visit: ${this.lastDateVisit}`;
        this.elem.btnShowMore.addEventListener('click', () => {
            this.showMore()
        });

        this.elem.btnHide.addEventListener('click', () => {
            this.hide();
        });

        if(parent){
            parent.append(this.elem.self)
        }else {
            return this.elem.self;
        }

        this.edit()
    }

    filledDoctor(dentist, formWrapper, modalForm) {
        super.filledDoctor(dentist, modalForm)
        dentist.lastDateVisit.value = `${this.lastDateVisit}`;
        dentist.renderEdit(formWrapper, this.id)
    }

    edit() {
        this.elem.editBtn.addEventListener('click', (e) => {
            e.preventDefault()
            const content = document.getElementById('content');
            const modalVisitEdit = new ModalVisit();
            modalVisitEdit.renderModalEdit(content)
            const formWrapper = document.querySelector('.form-wrapper-edit')
            const modalForm = document.querySelector('.form-edit')
            const dentist = new FormDentist();
            this.filledDoctor(dentist, formWrapper, modalForm)
            this.submitHandler(this.id)
        })

    }

    showMore(){
        const moreInfo =[];

        for (let key in this.elem) {
            if (key === 'purpose' || key === 'desc' || key === 'priority' || key === 'date' || key === "lastDateVisit") {
                moreInfo.push(this.elem[key]);
            }
        }
        moreInfo.forEach(item => {
            this.elem.self.insertBefore(item, this.elem.btnShowMore);
        });

        this.elem.btnShowMore.style.display = 'none';
        this.elem.btnHide.style.display = 'inline-block';
    }

    hide() {
        this.elem.self.removeChild(this.elem.purpose);
        this.elem.self.removeChild(this.elem.desc);
        this.elem.self.removeChild(this.elem.priority);
        this.elem.self.removeChild(this.elem.date);
        this.elem.self.removeChild(this.elem.lastDateVisit);
        this.elem.btnHide.style.display ='none';
        this.elem.btnShowMore.style.display = 'inline-block';
    }
}

export class VisitTherapist extends Visit {
    constructor({id, fullName, doctor, purpose, desc, priority, date, age}) {
        super({id, fullName, doctor, purpose, desc, priority, date});
        this.age = age;
    }

    render(parent){
        super.render(parent);
        this.elem.age = document.createElement('span');

        this.elem.age.textContent =`Age: ${this.age}`;

        this.elem.btnShowMore.addEventListener('click', () => {
            this.showMore()
        });

        this.elem.btnHide.addEventListener('click', () => {
            this.hide();
        });

        if(parent){
            parent.append(this.elem.self)
        }else {
            return this.elem.self;
        }
        this.edit()
    }

    showMore(){
        const moreInfo =[];

        for (let key in this.elem) {
            if (key === 'purpose' || key === 'desc' || key === 'priority' || key === 'date' || key === "age") {
                moreInfo.push(this.elem[key]);
            }
        }
        moreInfo.forEach(item => {
            this.elem.self.insertBefore(item, this.elem.btnShowMore);
        });

        this.elem.btnShowMore.style.display = 'none';
        this.elem.btnHide.style.display = 'inline-block';
    }

    hide() {
        this.elem.self.removeChild(this.elem.purpose);
        this.elem.self.removeChild(this.elem.desc);
        this.elem.self.removeChild(this.elem.priority);
        this.elem.self.removeChild(this.elem.date);
        this.elem.self.removeChild(this.elem.age);
        this.elem.btnHide.style.display ='none';
        this.elem.btnShowMore.style.display = 'inline-block';
    }

    filledDoctor(therapist, formWrapper, modalForm) {
        super.filledDoctor(therapist, modalForm)
        therapist.age.value = `${this.age}`;
        therapist.renderEdit(formWrapper, this.id)
    }

    edit() {
        this.elem.editBtn.addEventListener('click', (e) => {
            e.preventDefault()
            const content = document.getElementById('content');
            const modalVisitEdit = new ModalVisit();
            modalVisitEdit.renderModalEdit(content)
            const formWrapper = document.querySelector('.form-wrapper-edit')
            const modalForm = document.querySelector('.form-edit')
            const therapist = new FormTherapist();
            this.filledDoctor(therapist, formWrapper, modalForm)
            this.submitHandler(this.id)
        })

    }
}

