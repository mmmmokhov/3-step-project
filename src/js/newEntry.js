import {ModalLogin} from "./modal/Modal";
import {Filter} from "./Filter";
const cardWrapper = document.querySelector('.cardWrapper')
export const GHE = (selector) => document.querySelector(selector)
export default function newEntry() {
    if (localStorage.getItem('token') !== null) {
        GHE('.exit').style = "display:block;"
        GHE('.new-visit').style = "display:block;"
        GHE('.entry').style = "display:none;"
        GHE('.start__text').textContent = "";
        GHE('.d-flex').style = "background-image:none"
        GHE('.exit').addEventListener('click', exitLayaut);
        Filter('.filter')
    } else {
        const modal = GHE('.modal-login')
        const modalLogin = new ModalLogin().renderModal(modal)
        GHE('.entry').addEventListener('click', ()=>{
            GHE('.modal-form').style = "display:block;"
            const form = GHE("#form-login")
            form.addEventListener('submit', async function(e)  {
                e.preventDefault();
                const response = await fetch("https://ajax.test-danit.com/api/v2/cards/login", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({email: form.email.value, password: form.password.value})
                })
                if(response.ok){
                    const tocenText=await response.text()
                    await localStorage.setItem('token', tocenText)
                    GHE('.modal-login').innerHTML = ''
                    newEntry()
                }else{
                    GHE(".alert").style = "display:block;"
                }
            })
        })
    }}

function exitLayaut() {
    localStorage.removeItem('token');
    GHE('.start__text').textContent = "No items have been added";
    GHE('.entry').style = "display:block;"
    GHE('.new-visit').style = "display:none;"
    GHE('.exit').style = "display:none;"
    GHE('.filter').innerHTML = ''
    GHE('.cardWrapper').innerHTML = ''
    newEntry()
}
