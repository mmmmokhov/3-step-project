import {info} from "../utils/info";
import Input from "../componets/Input";
import TextArea from "../componets/TexAria";
import Form from "./Form.js"
import {createCard} from "../CardAPI";
import {filterSelect} from "../Filter";
import {getCards} from "../CardAPI";

export default class FormCardiologist extends Form {
    constructor(doctor) {
        super(doctor);
        this.doc = new Input(info.cardiologist, "form-control", 'doctor').create();
        this.pressure = new Input(info.pressure, "form-control", 'pressure').create();
        this.bodyMassIndex = new Input(info.weightIndex, "form-control", 'bodyMassIndex').create();
        this.diseases = new TextArea(info.illness, "form-control", 'diseases').create();
        this.age = new Input(info.age, "form-control", 'age').create();
    }

    render(modal) {
        super.render(modal);
        this.doc.defaultValue = 'Cardiologist';
        this.doc.readOnly = true;
        this.self.insertBefore(this.doc, this.fullName);
        this.self.insertBefore(this.pressure, this.submit);
        this.self.insertBefore(this.bodyMassIndex, this.submit);
        this.self.insertBefore(this.diseases, this.submit);
        this.self.insertBefore(this.age, this.submit);
        this.send()
    };
    renderEdit(modal, id) {
        super.renderEdit(modal, id);
        this.doc.defaultValue = 'Cardiologist';
        this.doc.readOnly = true;
        this.self.insertBefore(this.doc, this.fullName);
        this.self.insertBefore(this.pressure, this.submit);
        this.self.insertBefore(this.bodyMassIndex, this.submit);
        this.self.insertBefore(this.diseases, this.submit);
        this.self.insertBefore(this.age, this.submit);
    }
    send() {
        const readData = document.querySelector('.form-doctor');
        readData.addEventListener('submit', (e) => {
            e.preventDefault();

            const formData = new FormData(e.target);
            const data = Array.from(formData.entries()).reduce((memo, pair) => ({
                ...memo,
                [pair[0]]: pair[1],
            }), {});
            createCard(data)
                .then((response) => {
                    if (response.ok) {
                        const cardWrapper = document.querySelector('.cardWrapper')
                        cardWrapper.innerHTML = "";
                        filterSelect(getCards())
                    } else {
                        throw new Error('Something went wrong');
                    }

                })
            readData.reset();
            document.querySelector(".closeBtn").click();

        });
    }
};