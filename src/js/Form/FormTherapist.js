import {info} from "../utils/info";
import Input from "../componets/Input";
import Form from "./Form";
import {createCard, getCards} from "../CardAPI";
import renderCards from "../renderCards";
import {filterSelect} from "../Filter";

export default class FormTherapist extends Form {
    constructor(doctor) {
        super(doctor)
        this.doc = new Input(info.therapist, "form-control", 'doctor').create();
        this.age = new Input(info.age, "form-control", 'age').create();
    }

    render(modal) {
        super.render(modal);
        this.self.classList.add('form-therapist');
        this.doc.defaultValue = 'Therapist';
        this.doc.readOnly = true;
        this.self.insertBefore(this.doc, this.fullName);
        this.self.insertBefore(this.age, this.submit);
        this.send()
    }

    renderEdit(modal, id) {
        super.renderEdit(modal, id);
        this.doc.defaultValue = 'Therapist';
        this.doc.readOnly = true;
        this.self.insertBefore(this.doc, this.fullName);
        this.self.insertBefore(this.age, this.submit);
    }

    send() {
        const readData = document.querySelector('.form-doctor');
        readData.addEventListener('submit', (e) => {
            e.preventDefault();

            const formData = new FormData(e.target);
            const data = Array.from(formData.entries()).reduce((memo, pair) => ({
                ...memo,
                [pair[0]]: pair[1],
            }), {});
            createCard(data)
                .then((response) => {
                    if (response.ok) {
                        const cardWrapper = document.querySelector('.cardWrapper')
                        cardWrapper.innerHTML = "";
                        filterSelect(getCards())
                    } else {
                        throw new Error('Something went wrong');
                    }

                })
            readData.reset();
            document.querySelector(".closeBtn").click();
        });
    }
};
