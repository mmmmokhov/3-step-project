import {info} from "../utils/info";
import Input from "../componets/Input";
import TextArea from "../componets/TexAria";
import Select from "../componets/Select";

export default class Form {
    constructor(doctor) {
        this.self = document.createElement("form");
        this.self.classList.add('form-doctor');
        this.doctor = doctor;
        this.fullName = new Input(info.fullName, "form-control", 'fullName').create();
        this.purpose = new Input(info.purpose, "form-control", 'purpose').create();
        this.desc = new TextArea(info.desc, "form-control", 'desc').create();
        this.priority = new Select(info.priority, 'form-select', '', 'priority').create();
        this.date = new Input(info.visitDate, 'form-control', 'date').create();
        this.submit = new Input(info.submit, 'btn-primary-btn', 'btn').create();
    }

    render(modal) {
        this.date.setAttribute('data-placeholder', 'Visit Date');
        this.self.append(this.fullName, this.purpose, this.desc, this.priority, this.date, this.submit);
        modal.append(this.self);

    }

    renderEdit(modal, id) {
        this.self.classList.remove('form-doctor');
        this.self.classList.add('form-doctor-edit');
        this.self.append(this.fullName, this.purpose, this.desc, this.priority, this.date, this.submit);
        modal.append(this.self);
    }

}

